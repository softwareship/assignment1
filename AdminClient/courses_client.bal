import ballerina/io;

CoursesClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    CourseCode create_coursesRequest = {DSA621S: "ballerina"};
    Create_coursesStreamingClient create_coursesStreamingClient = check ep->create_courses();
    check create_coursesStreamingClient->sendCourseCode(create_coursesRequest);
    check create_coursesStreamingClient->complete();
    CourseDetails? create_coursesResponse = check create_coursesStreamingClient->receiveCourseDetails();
    io:println(create_coursesResponse);
}

