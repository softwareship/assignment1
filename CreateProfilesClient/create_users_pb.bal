import ballerina/grpc;
import ballerina/protobuf;

const string CREATE_USERS_DESC = "0A126372656174655F75736572732E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22230A0B5573657250726F66696C6512140A057469746C6518012001280952057469746C65224E0A0C55736572556E697175654964121A0A08756E6971756549641801200128055208756E69717565496412220A057469746C6518022001280B320C2E5573657250726F66696C6552057469746C6532430A1343726561746555736572735F53657276696365122C0A0B6372656174655F75736572120C2E5573657250726F66696C651A0D2E55736572556E6971756549642801620670726F746F33";

public isolated client class CreateUsers_ServiceClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, CREATE_USERS_DESC);
    }

    isolated remote function create_user() returns Create_userStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("CreateUsers_Service/create_user");
        return new Create_userStreamingClient(sClient);
    }
}

public client class Create_userStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUserProfile(UserProfile message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUserProfile(ContextUserProfile message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveUserUniqueId() returns UserUniqueId|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <UserUniqueId>payload;
        }
    }

    isolated remote function receiveContextUserUniqueId() returns ContextUserUniqueId|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <UserUniqueId>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public type ContextUserProfileStream record {|
    stream<UserProfile, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserProfile record {|
    UserProfile content;
    map<string|string[]> headers;
|};

public type ContextUserUniqueId record {|
    UserUniqueId content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: CREATE_USERS_DESC}
public type UserProfile record {|
    string title = "";
|};

@protobuf:Descriptor {value: CREATE_USERS_DESC}
public type UserUniqueId record {|
    int uniqueId = 0;
    UserProfile title = {};
|};

