import ballerina/io;

CreateUsers_ServiceClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    UserProfile create_userRequest = {title: "LEARNER"};
    Create_userStreamingClient create_userStreamingClient = check ep->create_user();
    check create_userStreamingClient->sendUserProfile(create_userRequest);
    check create_userStreamingClient->complete();
    UserUniqueId? create_userResponse = check create_userStreamingClient->receiveUserUniqueId();
    io:println(create_userResponse);
}

