import ballerina/grpc;
import ballerina/protobuf;

const string ASSIGNMENTS_DESC = "0A1161737369676E6D656E74732E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F222F0A1141737369676E6D656E744D657373616765121A0A08756E6971756549441801200128095208756E697175654944225D0A1141737369676E6D656E7444657461696C7312160A06776569676874180120012805520677656967687412300A136E756D6265724F6641737369676E6D656E747318022001280552136E756D6265724F6641737369676E6D656E7473324F0A0A41737369676E6D656E7412410A136E756D6265724F6641737369676E6D656E747312122E41737369676E6D656E744D6573736167651A122E41737369676E6D656E7444657461696C7328013001620670726F746F33";

public isolated client class AssignmentClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ASSIGNMENTS_DESC);
    }

    isolated remote function numberOfAssignments() returns NumberOfAssignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("Assignment/numberOfAssignments");
        return new NumberOfAssignmentsStreamingClient(sClient);
    }
}

public client class NumberOfAssignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssignmentMessage(AssignmentMessage message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssignmentMessage(ContextAssignmentMessage message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAssignmentDetails() returns AssignmentDetails|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <AssignmentDetails>payload;
        }
    }

    isolated remote function receiveContextAssignmentDetails() returns ContextAssignmentDetails|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AssignmentDetails>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public type ContextAssignmentMessageStream record {|
    stream<AssignmentMessage, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssignmentDetailsStream record {|
    stream<AssignmentDetails, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssignmentMessage record {|
    AssignmentMessage content;
    map<string|string[]> headers;
|};

public type ContextAssignmentDetails record {|
    AssignmentDetails content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: ASSIGNMENTS_DESC}
public type AssignmentMessage record {|
    string uniqueID = "";
|};

@protobuf:Descriptor {value: ASSIGNMENTS_DESC}
public type AssignmentDetails record {|
    int weight = 0;
    int numberOfAssignments = 0;
|};

