import ballerina/io;

AssignmentClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    AssignmentMessage numberOfAssignmentsRequest = {uniqueID: "ballerina"};
    NumberOfAssignmentsStreamingClient numberOfAssignmentsStreamingClient = check ep->numberOfAssignments();
    check numberOfAssignmentsStreamingClient->sendAssignmentMessage(numberOfAssignmentsRequest);
    check numberOfAssignmentsStreamingClient->complete();
    AssignmentDetails? numberOfAssignmentsResponse = check numberOfAssignmentsStreamingClient->receiveAssignmentDetails();
    io:println(numberOfAssignmentsResponse);
}

