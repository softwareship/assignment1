import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: ASSIGN_COURSES_SERVICE_DESC}
service "AssignCourse" on ep {

    remote function assign_course(stream<string, grpc:Error?> clientStream) returns stream<AssessorMessage, error?>|error {
        return {};
    }
}
