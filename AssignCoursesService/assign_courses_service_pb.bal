import ballerina/grpc;
import ballerina/protobuf;
import ballerina/protobuf.types.wrappers;

const string ASSIGN_COURSES_SERVICE_DESC = "0A1C61737369676E5F636F75727365735F736572766963652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F222D0A0F4173736573736F724D657373616765121A0A08756E6971756549441801200128095208756E69717565494432530A0C41737369676E436F7572736512430A0D61737369676E5F636F75727365121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A102E4173736573736F724D65737361676528013001620670726F746F33";

public isolated client class AssignCourseClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ASSIGN_COURSES_SERVICE_DESC);
    }

    isolated remote function assign_course() returns Assign_courseStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("AssignCourse/assign_course");
        return new Assign_courseStreamingClient(sClient);
    }
}

public client class Assign_courseStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(wrappers:ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAssessorMessage() returns AssessorMessage|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <AssessorMessage>payload;
        }
    }

    isolated remote function receiveContextAssessorMessage() returns ContextAssessorMessage|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AssessorMessage>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class AssignCourseAssessorMessageCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAssessorMessage(AssessorMessage response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAssessorMessage(ContextAssessorMessage response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextAssessorMessageStream record {|
    stream<AssessorMessage, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssessorMessage record {|
    AssessorMessage content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: ASSIGN_COURSES_SERVICE_DESC}
public type AssessorMessage record {|
    string uniqueID = "";
|};

