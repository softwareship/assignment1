import ballerina/grpc;
import ballerina/protobuf;

const string ASSIGN_COURSES_SERVICE_DESC = "0A1C61737369676E5F636F75727365735F736572766963652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22310A0F4173736573736F724D657373616765121E0A0A636F75727365436F6465180120012809520A636F75727365436F646522260A084173736573736F72121A0A08756E6971756549441801200128055208756E697175654944323E0A0C41737369676E436F75727365122E0A0D61737369676E5F636F7572736512102E4173736573736F724D6573736167651A092E4173736573736F723001620670726F746F33";

public isolated client class AssignCourseClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ASSIGN_COURSES_SERVICE_DESC);
    }

    isolated remote function assign_course(AssessorMessage|ContextAssessorMessage req) returns stream<Assessor, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        AssessorMessage message;
        if req is ContextAssessorMessage {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssignCourse/assign_course", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        AssessorStream outputStream = new AssessorStream(result);
        return new stream<Assessor, grpc:Error?>(outputStream);
    }

    isolated remote function assign_courseContext(AssessorMessage|ContextAssessorMessage req) returns ContextAssessorStream|grpc:Error {
        map<string|string[]> headers = {};
        AssessorMessage message;
        if req is ContextAssessorMessage {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssignCourse/assign_course", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        AssessorStream outputStream = new AssessorStream(result);
        return {content: new stream<Assessor, grpc:Error?>(outputStream), headers: respHeaders};
    }
}

public class AssessorStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|Assessor value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|Assessor value;|} nextRecord = {value: <Assessor>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public type ContextAssessorStream record {|
    stream<Assessor, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssessorMessage record {|
    AssessorMessage content;
    map<string|string[]> headers;
|};

public type ContextAssessor record {|
    Assessor content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: ASSIGN_COURSES_SERVICE_DESC}
public type AssessorMessage record {|
    string courseCode = "";
|};

@protobuf:Descriptor {value: ASSIGN_COURSES_SERVICE_DESC}
public type Assessor record {|
    int uniqueID = 0;
|};

