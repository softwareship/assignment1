import ballerina/io;

AssignCourseClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    AssessorMessage assign_courseRequest = {courseCode: "ballerina"};
    stream<Assessor, error?> assign_courseResponse = check ep->assign_course(assign_courseRequest);
    check assign_courseResponse.forEach(function(Assessor value) {
        io:println(value);
    });
}

