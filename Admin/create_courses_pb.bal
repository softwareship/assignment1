import ballerina/grpc;
import ballerina/protobuf;

const string CREATE_COURSES_DESC = "0A146372656174655F636F75727365732E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22260A0A436F75727365436F646512180A074453413632315318012001280952074453413632315322500A0D436F7572736544657461696C73121F0A04636F646518012001280B320B2E436F75727365436F64655204636F6465121E0A0A636F757273654E616D65180220012809520A636F757273654E616D65323A0A07436F7572736573122F0A0E6372656174655F636F7572736573120B2E436F75727365436F64651A0E2E436F7572736544657461696C732801620670726F746F33";

public isolated client class CoursesClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, CREATE_COURSES_DESC);
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("Courses/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourseCode(CourseCode message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourseCode(ContextCourseCode message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCourseDetails() returns CourseDetails|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <CourseDetails>payload;
        }
    }

    isolated remote function receiveContextCourseDetails() returns ContextCourseDetails|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <CourseDetails>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public type ContextCourseCodeStream record {|
    stream<CourseCode, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseDetails record {|
    CourseDetails content;
    map<string|string[]> headers;
|};

public type ContextCourseCode record {|
    CourseCode content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: CREATE_COURSES_DESC}
public type CourseDetails record {|
    CourseCode code = {};
    string courseName = "";
|};

@protobuf:Descriptor {value: CREATE_COURSES_DESC}
public type CourseCode record {|
    string DSA621S = "";
|};

