import ballerina/grpc;

listener grpc:Listener epp = new (9090);

@grpc:Descriptor {value: CREATE_COURSES_DESC}
service "Courses" on epp {

    remote function create_courses(stream<CourseCode, grpc:Error?> clientStream) returns CourseDetails|error {
        return {code: {}, courseName: "Distributed Systems"};
    }
}

