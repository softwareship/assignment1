import ballerina/grpc;
import ballerina/protobuf;

const string LEARNER_DESC = "0A0D6C6561726E65722E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22CD010A0D436F757273654D657373616765121A0A08756E6971756549441801200128095208756E69717565494412300A136E756D6265724F6641737369676E6D656E747318022001280552136E756D6265724F6641737369676E6D656E747312360A167765696768744F664561636841737369676E6D656E7418032001280552167765696768744F664561636841737369676E6D656E7412360A1661737369676E436F75727365546F4173736573736F72180420012809521661737369676E436F75727365546F4173736573736F72225B0A0F4C6561726E657273436F7572736573122C0A1172656769737465726564436F7572736573180120012809521172656769737465726564436F7572736573121A0A08756E6971756549641802200128095208756E69717565496432450A074C6561726E6572123A0A127265676973746572466F7241436F75727365120E2E436F757273654D6573736167651A102E4C6561726E657273436F757273657328013001620670726F746F33";

public isolated client class LearnerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, LEARNER_DESC);
    }

    isolated remote function registerForACourse() returns RegisterForACourseStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("Learner/registerForACourse");
        return new RegisterForACourseStreamingClient(sClient);
    }
}

public client class RegisterForACourseStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourseMessage(CourseMessage message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourseMessage(ContextCourseMessage message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveLearnersCourses() returns LearnersCourses|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <LearnersCourses>payload;
        }
    }

    isolated remote function receiveContextLearnersCourses() returns ContextLearnersCourses|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <LearnersCourses>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class LearnerLearnersCoursesCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendLearnersCourses(LearnersCourses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextLearnersCourses(ContextLearnersCourses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextLearnersCoursesStream record {|
    stream<LearnersCourses, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseMessageStream record {|
    stream<CourseMessage, error?> content;
    map<string|string[]> headers;
|};

public type ContextLearnersCourses record {|
    LearnersCourses content;
    map<string|string[]> headers;
|};

public type ContextCourseMessage record {|
    CourseMessage content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: LEARNER_DESC}
public type LearnersCourses record {|
    string registeredCourses = "";
    string uniqueId = "";
|};

@protobuf:Descriptor {value: LEARNER_DESC}
public type CourseMessage record {|
    string uniqueID = "";
    int numberOfAssignments = 0;
    int weightOfEachAssignment = 0;
    string assignCourseToAssessor = "";
|};

