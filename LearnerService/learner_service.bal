import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: LEARNER_DESC}
service "Learner" on ep {

    remote function registerForACourse(stream<CourseMessage, grpc:Error?> clientStream) returns stream<LearnersCourses, error?>|error {
    }
}

