import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: ASSIGNMENTS_DESC}
service "Assignment" on ep {

    remote function numberOfAssignments(stream<AssignmentMessage, grpc:Error?> clientStream) returns stream<AssignmentDetails, error?>|error {
    }
}

