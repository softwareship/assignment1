import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: CREATE_USERS_DESC}
service "CreateUsers_Service" on ep {

    remote function create_user(stream<UserProfile, grpc:Error?> clientStream) returns UserUniqueId|error {
        return {uniqueId: 220034311, title: {}};
    }
}

