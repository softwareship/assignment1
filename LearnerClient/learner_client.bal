import ballerina/io;

LearnerClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    CourseMessage registerForACourseRequest = {uniqueID: "ballerina", numberOfAssignments: 1, weightOfEachAssignment: 1, assignCourseToAssessor: "ballerina"};
    RegisterForACourseStreamingClient registerForACourseStreamingClient = check ep->registerForACourse();
    check registerForACourseStreamingClient->sendCourseMessage(registerForACourseRequest);
    check registerForACourseStreamingClient->complete();
    LearnersCourses? registerForACourseResponse = check registerForACourseStreamingClient->receiveLearnersCourses();
    io:println(registerForACourseResponse);
}

